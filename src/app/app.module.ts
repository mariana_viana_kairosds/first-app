import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { AuthService } from './auth.service';
import { LoginComponent } from './auth/login/login.component';
import { CustomPipesModule } from './custom-pipes/custom-pipes.module';

const ROUTES: Routes = [
  {path: '', redirectTo: 'directive', pathMatch: 'full'},
  {path: 'login', component: LoginComponent},
  {path: 'directive', loadChildren: () => import('./directive/directive.module').then(m => m.DirectiveModule)},
  {path: 'databinding', canActivate:[AuthService], loadChildren: () => import('./databinding/databinding.module').then(m => m.DatabindingModule)},
  {path: '**', redirectTo:'directive'}
]

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    CustomPipesModule,
    RouterModule.forRoot(ROUTES)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
