import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ConfirmService } from '../confirm.service';
import { ChildComponent } from './child/child.component';
import { FatherComponent } from './father/father.component';

const ROUTES: Routes = [
  {path:'', component: FatherComponent},
  {path:'child/:id', canDeactivate: [ConfirmService], component: ChildComponent}
];

@NgModule({
  declarations: [
    FatherComponent,
    ChildComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(ROUTES)
  ],
  exports: [FatherComponent]
})
export class DatabindingModule { }
